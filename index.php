<?php

// Includes
//include('RssReader.php');
// Anfangswerte
// zu durchsuchende URL
define(PROJECT_NAME, 'WebCrawler');
define(PROJECT_VERSION, 'v0.1.0');
$url = 'http://www.kleinezeitung.at/rss/chronik';
$web_agency_id = 'f4d8d9a1';

// Quelltext der Website aufrufen
$hp = file_get_contents($url);

// Pattern für Links
$regex = "/<item.*>(.*)<\\/item>/Uism"; ///<link>(.*)\\?from\\=rss<\\/link>/Uim

// Zeilenumbrüche entfernen
//$hp = str_replace("\n", "", $hp);

// durch Pattern laufen lassen
preg_match_all($regex, $hp, $matches);

// Aufbereiten des Strings
$mod_data = array();
for ($iter = 0; $iter < count($matches[1]); $iter++) {
  // Nachrichtenargentur
  $mod_data[$iter]['agency'] = $web_agency_id;
  // Titel Filtern
  preg_match_all("/<title>(.*)<\\/title>/Uism", $matches[1][$iter], $title);
  $mod_data[$iter]['title'] = $title[1][0];

  // Link Filtern
  preg_match_all("/<link>(.*)\\?from\\=rss<\\/link>/Uism", $matches[1][$iter], $link);
  $mod_data[$iter]['link'] = $link[1][0];

  // Beschreibung Filtern
  preg_match_all("/<description>(.*)<\\/description>/Uism", $matches[1][$iter], $description);
  $mod_data[$iter]['description'] = $description[1][0];

  // Bild Filtern
  preg_match_all("/<enclosure url\\=\"(.*)\"(.*)\\/>/Uism", $matches[1][$iter], $img);
  $mod_data[$iter]['image'] = $img[1][0];

  // Datum Filtern
  preg_match_all("/<dc\\:date>(.*)<\\/dc\\:date>/Uism", $matches[1][$iter], $date);
  $mod_data[$iter]['date'] = strtotime($date[1][0]);
}

$urls = array();
for ($iter = 0; $iter < count($mod_data); $iter++) {
  $urls[$iter] = $mod_data[$iter]['link'];
}
//var_dump($mod_data[0]);
//echo json_encode($urls, JSON_UNESCAPED_SLASHES);
//return true;

//getRssFeed($url, "kleine", "auto", 10, 3);

?>
<!DOCTYPE html>
<html>
  <head>
    <meta charset="utf-8">
    <title><?php echo PROJECT_NAME; ?></title>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.24.2/css/uikit.min.css" />
  </head>
  <body>
      <div class="uk-panel uk-panel-box uk-panel-box-primary uk-margin">
        <h3 class="uk-panel-title">RSS Kanal</h3>
        <a href="<?php echo $url; ?>">Kleine Zeitung</a>
      </div>
      <?php
      foreach($mod_data as &$data) {
        echo '
          <div class="uk-panel uk-panel-box uk-panel-box-primary uk-margin">
          <article class="uk-comment">
            <header class="uk-comment-header">
              <img class="uk-comment-avatar" src="'.$data['image'].'" alt="" style="width: 100px;">
              <h4 class="uk-comment-title">'.$data['title'].'</h4>
              <ul class="uk-comment-meta uk-subnav uk-subnav-line">
                <li class="uk-disabled"><a href="#">'.date("d.m.Y H:m:s", $data['date']).'</a></li>
                <li><a href="'.$data['link'].'">Website</a></li>
              </ul>
            </header>
            <div class="uk-comment-body">'.$data['description'].'</div>
          </article>
          </div>
        ';
      }
      unset($data);
      ?>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.4/jquery.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/uikit/2.24.2/js/uikit.min.js"></script>
  </body>
</html>
