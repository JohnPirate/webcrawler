<?php

error_reporting(E_ALL);
ini_set('display_errors', true);

require_once __DIR__ . '/vendor/autoload.php';

$pimple = new \Pimple\Container();

require_once __DIR__ . '/pimple.php';
