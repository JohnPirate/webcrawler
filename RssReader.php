<?php
function getRssFeed($rssfeed, $cssclass="", $encode="auto", $anzahl=10, $mode=0) {
  $data = @file($rssfeed);
  $data = implode ("", $data);
  preg_match_all("/<item.*>(.+)</item>/Uism", $data, $items);

  if($encode == "auto")
  {
    preg_match("/<?xml.*encoding="(.+)".*?>/Uism", $data, $encodingarray);
    $encoding = $encodingarray[1];
  }
  else
  {
    $encoding = $encode;
  }

  echo '<div class="rssfeed_'.$cssclass.'">';
  // Titel und Link zum Channel
  if($mode == 1 || $mode == 3)
  {
    $data = preg_replace("/<item>(.+)</item>/Uism", '', $data);
    preg_match("/<title>(.+)</title>/Uism", $data, $channeltitle);
    preg_match("/<link>(.+)</link>/Uism", $data, $channellink);

    $channeltitle = preg_replace('/<![CDATA[(.+)]]>/Uism', '$1', $channeltitle);
    $channellink = preg_replace('/<![CDATA[(.+)]]>/Uism', '$1', $channellink);

    echo '<h1><a href="'.$channellink[1].'" title="';
    if($encode != "no")
    {
      echo htmlentities($channeltitle[1],ENT_QUOTES,$encoding);
    }
    else
    {
      echo $channeltitle[1];
    }
    echo '">';
    if($encode != "no")
    {
      echo htmlentities($channeltitle[1],ENT_QUOTES,$encoding);
    }
    else
    {
      echo $channeltitle[1];
    }
    echo '</a></h1><br>';
  }

  // Titel, Link und Beschreibung der Items
  foreach ($items[1] as $item)
  {
    preg_match("/<title>(.+)</title>/Uism", $item, $title);
    preg_match("/<link>(.+)</link>/Uism", $item, $link);
    preg_match("/<description>(.*)</description>/Uism", $item, $description);

    $title = preg_replace('/<![CDATA[(.+)]]>/Uism', '$1', $title);
    $description = preg_replace('/<![CDATA[(.+)]]>/Uism', '$1', $description);
    $link = preg_replace('/<![CDATA[(.+)]]>/Uism', '$1', $link);

    echo '<p class="link"><br>';
    echo '<a href="'.$link[1].'" title="';
    if($encode != "no")
    {
      echo htmlentities($title[1],ENT_QUOTES,$encoding);
    }
    else
    {
      echo $title[1];
    }
    echo '">';
    if($encode != "no")
    {
      echo htmlentities($title[1],ENT_QUOTES,$encoding).'</a><br>';
    }
    else
    {
      echo $title[1].'</a><br>';
    }
    echo '</p><br>';
    if($mode == 2 || $mode == 3 && ($description[1]!="" && $description[1]!=" "))
    {
      echo '<p class="description"><br>';
      if($encode != "no")
      {
        echo htmlentities($description[1],ENT_QUOTES,$encoding).'<br>';
      }
      else
      {
        echo $description[1];}
        echo '</p><br>';
    }
    if ($anzahl-- <= 1) break;
  }
  echo '</div>';
}
?>
